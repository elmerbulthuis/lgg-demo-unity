# LatencyGG Probe Library for C\#

A C\# library for latency measurements using the LatencyGG network.

## Usage
See the example for recommended usage.

### QuickStart
First create an instance of LatencyGG specifying the type of `AF` (`eINET` for IPv4 `eINET6` for IPV6) and the `timeout` limit for pings.
```c#
LatencyGG.LatencyGG latency = new LatencyGG.LatencyGG(LatencyGG.AF.eINET, 1000);
```
Then create instances of DataPing, specifying the `target_ip` (beacon address), `own_ip` (user public ip) and `ident`(as provided by the Latency.gg Client API) 
```c#
LatencyGG.DataPing dataPing = new LatencyGG.DataPing(targetIp, ownIp, ident);
```
Next, `add` all of the pings you wish to run simultaneously (for accurate measurements don't run more than 10 measurements simultaneously). 
```c#
latency.Add(ping1);
```
Finally, `run` the measurement.
```c#
latency.Run();
```
If you need to terminate a running measurement before it is complete, discarding the measurements call `Kill()`.
```c#
latency.Kill();
```
To check if all measurements are complete you can poll the `IsComplete()` method.
```c#
if (latency.IsComplete()) {
    // Do something
}
```
Results can be retrieved via a Map-like interface
```c#
LatencyGG.DataPing pingResult = latency[targetIp];
Console.Write(pingResult.GetStats().mRtt.ToString());