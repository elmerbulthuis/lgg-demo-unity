using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Beacon : MonoBehaviour
{
    // Start is called before the first frame update
    LineRenderer lr;
    void Start()
    {
        GameObject circle = GameObject.Find("Probe");
        Material lineMat = new Material(Shader.Find("Unlit/Color"));
        Color lineColor = new Color(0.9059f, 0.7176f, 0.3961f, 1.0f);
        lineMat.SetColor("_Color", lineColor);
        lr = this.gameObject.AddComponent<LineRenderer>();
        lr.material = lineMat;
        lr.SetPosition(0, circle.transform.position);
        lr.SetPosition(1, this.gameObject.transform.position);
        lr.startWidth = .01f;
        lr.endWidth = .01f;
    }

    // Update is called once per frame
    void Update()
    {
        GameObject circle = GameObject.Find("Probe");
        lr.SetPosition(0, circle.transform.position);
        lr.SetPosition(1, this.gameObject.transform.position);
        float distance = Vector3.Distance(circle.transform.position, this.gameObject.transform.position);
        Material beaconMat = new Material(Shader.Find("Unlit/Color"));
        Color beaconColorA = new Color(0.21f, 0.3098f, 0.4802f, 1.0f);
        Color beaconColorB = new Color(0.3098f, 0.4802f, 0.21f, 1.0f);
        Color beaconColor = Color.Lerp(beaconColorB, beaconColorA, distance/4.5f);
        beaconMat.SetColor("_Color", beaconColor);
        this.gameObject.GetComponent<Renderer>().material = beaconMat;
    }

    ~Beacon()
    {
        Destroy(this.gameObject);
    }
}
