using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using TMPro;
using Newtonsoft.Json;


public class FetchBeacons : MonoBehaviour
{
    public GameObject beaconPrefab;
    public GameObject probePrefab;
    public Text outputText;
    bool initialising = false;
    bool running = false;
    LatencyGG.LatencyGG latency = new LatencyGG.LatencyGG();
    Dictionary<String, List<String>> lookupIP = new Dictionary<string, List<string>>();
    Dictionary<String, GameObject> lookupSquare = new Dictionary<string, GameObject>();
    Dictionary<String, double> lookupAngle = new Dictionary<string, double>();
    Dictionary<String, double> lookupDistance = new Dictionary<string, double>();
    Dictionary<String, double> lookupRTT = new Dictionary<string, double>();
    LatencyGGDemoAPI apiResponse;
    String ident; 
    String addr;
    double maxLatency = 500;

    // Start is called before the first frame update
    void Start()
    {
        spawnProbe();
        String url = "https://demo.latency.gg/metrics";
        String response;
        using (WebClient client = new WebClient())
        {
            response = client.DownloadString(url);
        }
        apiResponse = JsonConvert.DeserializeObject<LatencyGGDemoAPI>(response);
        ident = apiResponse.source.ident;
        addr = apiResponse.source.addr;
        running = true;
    }

    private void initialiseLatency()
    {
        initialising = true;
        latency = new LatencyGG.LatencyGG(LatencyGG.AF.eINET, 1000);
        lookupIP = new Dictionary<string, List<string>>();
       
        double flat_count = 0;

        foreach ((String provider_name, var location) in apiResponse.stale_metrics)
        {
            foreach ((String location_name, LatencyGGDemoMetrics metric) in location)
            {
                foreach (LatencyGGDemoBeacon beacon in metric.beacons)
                {
                    if (!lookupIP.ContainsKey(beacon.ipv4))
                    {
                        LatencyGG.DataPing dataping = new LatencyGG.DataPing(beacon.ipv4, addr, ident);
                        latency.Add(dataping);
                        lookupIP[beacon.ipv4] = new List<string>();
                        lookupIP[beacon.ipv4].Add(provider_name);
                        lookupIP[beacon.ipv4].Add(location_name);
                        flat_count++;
                    }
                }
            }
        }
        foreach ((String provider_name, var location) in apiResponse.clean_metrics)
        {
            foreach ((String location_name, LatencyGGDemoMetrics metric) in location)
            {
                foreach (LatencyGGDemoBeacon beacon in metric.beacons)
                {
                    if (!lookupIP.ContainsKey(beacon.ipv4))
                    {
                        LatencyGG.DataPing dataping = new LatencyGG.DataPing(beacon.ipv4, addr, ident);
                        latency.Add(dataping);
                        lookupIP[beacon.ipv4] = new List<string>();
                        lookupIP[beacon.ipv4].Add(provider_name);
                        lookupIP[beacon.ipv4].Add(location_name);
                        flat_count++;
                    }
                }
            }
        }
        int i = 0;
        foreach (String ipv4 in latency.GetKeys())
        {
            if (!lookupSquare.ContainsKey(ipv4))
            {
                spawnBeacon(4.5, i * (360 / flat_count), ipv4);
                i++;
            }
        }
        initialising = false;
    }

    private void spawnProbe()
    {
        GameObject a = Instantiate(probePrefab) as GameObject;
        a.transform.position = new Vector2(0, 0);
        a.name = "Probe";
    }

    private void spawnBeacon(double distance, double bearing, String ip)
    {
        GameObject a = Instantiate(beaconPrefab) as GameObject;
        double radians = (bearing * (Math.PI)) / 180;
        double x = distance * Math.Sin(radians);
        double y = distance * Math.Cos(radians);
        a.transform.position = new Vector2((float)x, (float)y);
        GameObject textobj = a.gameObject.transform.GetChild(0).gameObject;
        TextMeshPro t = textobj.GetComponent<TextMeshPro>();
        t.text = lookupIP[ip][0] + "\n" + lookupIP[ip][1];
        lookupAngle[ip] = radians;
        lookupSquare[ip] = a;
    }

    private void updateBeaconLocations()
    {
        foreach ((string ip, double target_distance) in lookupDistance)
        {
            if (lookupSquare.ContainsKey(ip) && lookupAngle.ContainsKey(ip))
            {
                GameObject a = lookupSquare[ip];
                double radians = lookupAngle[ip];
                double old_distance = Math.Sqrt(Math.Pow(a.transform.position[0], 2) + Math.Pow(a.transform.position[1], 2));
                double new_distance = (old_distance*59 + target_distance) / 60;
                double x = new_distance * Math.Sin(radians);
                double y = new_distance * Math.Cos(radians);
                a.transform.position = new Vector2((float)x, (float)y);
                GameObject textobj = a.gameObject.transform.GetChild(0).gameObject;
                TextMeshPro t = textobj.GetComponent<TextMeshPro>();
                t.text = lookupIP[ip][0] + "\n" + lookupIP[ip][1] + "\n" + String.Format("{0:0}", lookupRTT[ip]) + "ms";
            }
        }
    }

    private void updateBeacons()
    {
        double newMaxLatency = 0;
        foreach (String ip in latency.GetKeys())
        {
            double rtt;
            if (latency[ip].IsDead())
            {
                rtt = 1000;
            }
            else
            {
                rtt = latency[ip].GetStats().mRtt;
            }
            newMaxLatency = Math.Max(newMaxLatency, rtt); 
        }
        maxLatency = (maxLatency + newMaxLatency*3)/4;

        foreach (String ip in latency.GetKeys()) {
            double rtt;
            if (latency[ip].IsDead())
            {
                rtt = 1000;
            }
            else
            {
                rtt = latency[ip].GetStats().mRtt;
            }
            double distance = (rtt / maxLatency * 4) + 0.5;
            if (lookupSquare.ContainsKey(ip))
            {
                lookupDistance[ip] = distance;
                lookupRTT[ip] = rtt;
            }
            
        }
    }

    private async Task PrepareMeasurement()
    {
        initialiseLatency();
        await Task.Run(latency.Run);
    }

    async void Update()
    {
        if ((Keyboard.current != null && Keyboard.current.escapeKey.wasPressedThisFrame) || (Gamepad.current != null && Gamepad.current.bButton.wasPressedThisFrame))
        {
            CloseButton();
        }
        if (!initialising && running)
        {
            if (latency.IsComplete())
            {
                updateBeacons();
            }
            if (!latency.IsRunnable())
            {
                await PrepareMeasurement();
            }
            updateBeaconLocations();
        }
    }

    public void CloseButton()
    {
        latency.Kill();
        running = false;
        while (latency.IsRunnable())
        {
            Debug.Log("Waiting");
        }
        SceneManager.LoadScene("Menu");
    }
}


static class KvpExtensions
{
    public static void Deconstruct<TKey, TValue>(
        this KeyValuePair<TKey, TValue> kvp,
        out TKey key,
        out TValue value)
    {
        key = kvp.Key;
        value = kvp.Value;
    }
}

[System.Serializable]
public class LatencyGGDemoAPI
{
    public LatencyGGDemoSource source;
    public Dictionary<String, Dictionary<String, LatencyGGDemoMetrics>> clean_metrics;
    public Dictionary<String, Dictionary<String, LatencyGGDemoMetrics>> stale_metrics;
}

[System.Serializable]
public class LatencyGGDemoSource
{
    public String addr;
    public int version;
    public String ident;
}

[System.Serializable]
public class LatencyGGDemoMetrics
{
    public List<LatencyGGDemoBeacon> beacons;
    public double rtt;
    public double stddev;
    public bool stale;
}

[System.Serializable]
public class LatencyGGDemoBeacon
{
    public String ipv4;
    public String ipv6;
}


[System.Serializable]
public class GeoIPNetworkAPI
{
    public String prefix;
    public String region;
    public String allocated_cc;
    public String asn;
    public String rir;
    [JsonProperty(PropertyName = "as-name")]
    public String as_name;
    public GeoJSON geo;
    public Double radius;
    public String timestamp;
}

[System.Serializable]
public class GeoJSON
{
    public String type;
    public GeoJSONProperties properties;
    public GeoJSONGeometry geometry;
}

[System.Serializable]
public class GeoJSONGeometry
{
    public String type;
    public List<Double> coordinates;
}

[System.Serializable]
public class GeoJSONProperties
{
    public Double radius;
}