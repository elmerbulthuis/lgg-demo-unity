﻿using System;

namespace LatencyGG
{
    class TimestampMS
    {
        public static UInt64 Now()
        {
            return Convert.ToUInt64(DateTimeOffset.Now.ToUnixTimeMilliseconds());
        }
    }
}