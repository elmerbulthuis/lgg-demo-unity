﻿using System;

namespace LatencyGG
{
    public enum DataPingState
    {
        eKilled = -2,
        eEmpty = -1,
        eC0 = 0,
        eS0,
        eC1,
        eS1,
        eC2,
        eComplete
    }
    public class DataPing
    {
        DataPingState mState = DataPingState.eC0;
        String mVersion = "v0.0.0-local";
        String mIdent;
        public UInt64 mTimestampC0, mTimestampS0, mTimestampC1, mTimestampS1, mTimestampC2, mTimestampS2, mTimestampC3;
        String[] mSignatures = new String[3];
        UInt16 mSeq;
        public UInt16 mErrors;
        public String mPrint;
        public String mTargetIp;
        public String mSourceIp;
        public DataPing()
        {
            mState = DataPingState.eEmpty;
            mIdent = "";
            mTimestampC0 = mTimestampS0 = mTimestampC1 = mTimestampS1 = mTimestampC2 = mTimestampS2 = mTimestampC3 = 0;
            mSeq = 0;
            mErrors = 0;
            mTargetIp = "";
            mSourceIp = "";
        }

        public DataPing(String aTargetIp, String aSourceIp, String aIdent)
        {
            mState = DataPingState.eC0;
            mIdent = aIdent;
            mTimestampC0 = mTimestampS0 = mTimestampC1 = mTimestampS1 = mTimestampC2 = mTimestampS2 = mTimestampC3 = TimestampMS.Now();
            Random rand = new Random();
            mSeq = (UInt16)rand.Next(0, 255);
            mErrors = 0;
            mTargetIp = aTargetIp;
            mSourceIp = aSourceIp;
        }

        public void Overwrite(DataPing aDataPing)
        {
            mTimestampC0 = aDataPing.mTimestampC0;
            mTimestampC1 = aDataPing.mTimestampC1;
            mTimestampC2 = aDataPing.mTimestampC2;
            mTimestampC3 = aDataPing.mTimestampC3;
            mTimestampS0 = aDataPing.mTimestampS0;
            mTimestampS1 = aDataPing.mTimestampS1;
            mTimestampS2 = aDataPing.mTimestampS2;
            for (int i = 0; i < 3; i++)
            {
                mSignatures[i] = aDataPing.mSignatures[i];
            }
            mErrors = aDataPing.mErrors;
            mVersion = aDataPing.mVersion;
            mSourceIp = aDataPing.mSourceIp;
            mIdent = aDataPing.mIdent;
            mState = aDataPing.mState;
            mSeq = aDataPing.mSeq;
        }

        public void Update(Byte[] aReply, int aReplyLength)
        {
            if (mErrors >= 10)
            {
                Kill();
                return;
            }
            DataPingResponse inPacket = DataPingResponse.Deserialize(aReply);
            if (mSeq != inPacket.mSeq)
            {
                //mPrint = "Sequence Mismatch: " + mSeq + " != " + inPacket.mSeq + " ||" + inPacket.mType + " " + inPacket.mSig;
                mErrors++;
                return;
            }
            switch (inPacket.mType)
            {
                case DataPingResponseType.eNull:
                    mErrors++;
                    break;
                case DataPingResponseType.eInit:
                    mState = DataPingState.eS0;
                    mTimestampS0 = mTimestampC1 = mTimestampS1 = mTimestampC2 = mTimestampS2 = mTimestampC3 = inPacket.mTimestamp;
                    mSignatures[0] = inPacket.mSig;
                    break;
                case DataPingResponseType.eNext:
                    mState = DataPingState.eS1;
                    mTimestampS1 = mTimestampC2 = mTimestampS2 = mTimestampC3 = inPacket.mTimestamp;
                    mSignatures[1] = inPacket.mSig;
                    break;
                case DataPingResponseType.eFinal:
                    mState = DataPingState.eComplete;
                    mTimestampS2 = inPacket.mTimestamp;
                    mSignatures[2] = inPacket.mSig;
                    mTimestampC3 = TimestampMS.Now();
                    break;
            }
        }

        public Byte[] GeneratePacket()
        {
            DataPingRequest outPacket = new DataPingRequest();
            outPacket.mVersion = 2;
            outPacket.mIdent = mIdent;
            outPacket.mSeq = mSeq;
            switch (mState)
            {
                case DataPingState.eC0:
                    mTimestampC0 = TimestampMS.Now();
                    outPacket.mTimestamp = mTimestampC0;
                    outPacket.mType = DataPingRequestType.eInit;
                    break;
                case DataPingState.eS0:
                case DataPingState.eC1:
                    mTimestampC1 = TimestampMS.Now();
                    outPacket.mTimestamp = mTimestampC1;
                    outPacket.mType = DataPingRequestType.eSecond;
                    break;
                case DataPingState.eS1:
                case DataPingState.eC2:
                    mTimestampC2 = TimestampMS.Now();
                    outPacket.mTimestamp = mTimestampC2;
                    outPacket.mType = DataPingRequestType.eFinal;
                    break;
                case DataPingState.eKilled:
                case DataPingState.eEmpty:
                case DataPingState.eComplete:
                    throw new IndexOutOfRangeException("tried to generate packet from invalid state");
            }
            var outBytes = outPacket.Serialize();
            mPrint = Convert.ToBase64String(outBytes);
            return outBytes;
        }

        public Boolean IsComplete()
        {
            return mState == DataPingState.eComplete;
        }

        public Boolean IsDead()
        {
            return mState == DataPingState.eKilled;
        }

        public Boolean IsEmpty()
        {
            return mState == DataPingState.eEmpty;
        }

        public Stats GetStats()
        {
            Stats stats = new Stats();

            double[] samples = new double[5];
            samples[0] = (mTimestampC1 - mTimestampC0);
            samples[1] = (mTimestampC2 - mTimestampC1);
            samples[2] = (mTimestampC3 - mTimestampC2);
            samples[3] = (mTimestampS1 - mTimestampS0);
            samples[4] = (mTimestampS2 - mTimestampS1);
            double sum = 0;
            foreach (double x in samples)
            {
                sum += x;
            }
            stats.mRtt = Convert.ToUInt64(sum) / 5;
            double varience = 0;
            foreach (UInt64 x in samples)
            {
                varience += Math.Pow((x - stats.mRtt), 2);
            }
            stats.mStddev = Convert.ToUInt64(Math.Sqrt(varience) / 5);
            return stats;
        }

        public void Kill()
        {
            mState = DataPingState.eKilled;
            mTimestampC0 = 0;
            mTimestampS0 = 500;
            mTimestampC1 = 1000;
            mTimestampS1 = 1500;
            mTimestampC2 = 2000;
            mTimestampS2 = 2500;
            mTimestampC3 = 3000;
        }

    }
}