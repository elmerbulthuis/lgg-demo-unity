﻿using System;

namespace LatencyGG
{
    public struct Stats
    {
        public UInt64 mRtt;
        public UInt64 mStddev;
    }
}