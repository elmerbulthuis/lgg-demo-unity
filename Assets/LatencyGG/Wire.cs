﻿using System;
using System.Runtime.InteropServices;

namespace LatencyGG
{
    public enum DataPingResponseType : Int32
    {
        eNull = 0,
        eInit,
        eNext,
        eFinal = 255
    }

    public enum DataPingRequestType : Int32
    {
        eInit = 1,
        eSecond,
        eFinal = 255
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    struct DataPingRequest
    {
        public Int32 mVersion;
        public DataPingRequestType mType;
        public UInt16 mSeq;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 89)]
        public String mIdent;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 5)]
        public String UNUSED;
        public UInt64 mTimestamp;

        public Byte[] Serialize()
        {
            UInt16 buffSize = 112;
            Byte[] output = new Byte[buffSize];
            IntPtr handle = Marshal.AllocHGlobal(buffSize);
            Marshal.StructureToPtr(this, handle, false);
            Marshal.Copy(handle, output, 0, buffSize);
            Marshal.FreeHGlobal(handle);
            return output;
        }
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    struct DataPingResponse
    {
        public Int32 mVersion;
        public DataPingResponseType mType;
        public UInt16 mSeq;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 89)]
        public String mSig;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 5)]
        public String UNUSED;
        public UInt64 mTimestamp;

        public static DataPingResponse Deserialize(Byte[] aData)
        {
            UInt16 buffSize = 112;
            DataPingResponse dataPingResponse;
            IntPtr handle = Marshal.AllocHGlobal(buffSize);
            Marshal.Copy(aData, 0, handle, buffSize);
            dataPingResponse = Marshal.PtrToStructure<DataPingResponse>(handle);
            return dataPingResponse;
        }
    }
}